var select_capacidade = document.querySelector("#select_capacidade");
var select_capacidade_2 = document.querySelector("#select_capacidade_2");
var input_capacidade1 = document.querySelector("#input_capacidade1");
var input_capacidade_2 = document.querySelector("#input_capacidade2");
var capacidade = document.querySelector("#capacidade");
select_valor1 = select_capacidade.value;
select_valor2 = select_capacidade_2.value;
select_valor1 = parseInt(select_valor1);
select_valor2 = parseInt(select_valor2);
var p = document.createElement("p");

select_capacidade.addEventListener("change", function() {
    select_valor1 = this.value;
    select_valor1 = parseInt(select_valor1);
    converter_capacidade(valor_input_capacidade1, select_valor1, select_valor2);
    return select_valor1;
});

select_capacidade_2.addEventListener("change", function() {
    select_valor2 = this.value;
    select_valor2 = parseInt(select_valor2);
    converter_capacidade(valor_input_capacidade1, select_valor1, select_valor2);
    return select_valor2;
});

input_capacidade1.addEventListener("keyup", function() {
    if (this.value == "") {
        input_capacidade_2.value = "";
    } else {
        valor_input_capacidade1 = this.value;
        valor_input_capacidade1 = valor_input_capacidade1.replace(/,/i, ".");
        console.log(valor_input_capacidade1);
        valor_input_capacidade1 = parseFloat(valor_input_capacidade1);
        converter_capacidade(valor_input_capacidade1, select_valor1, select_valor2);
    }
    return valor_input_capacidade1;
});

function converter_capacidade(valor, select1, select2) {
    switch (select1) {
        case 1: //INICIO KM
            switch (select2) {
                case 1:
                    input_capacidade_2.value = valor;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    break;
                case 2:
                    input_capacidade_2.value = valor * 10;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar quilolitro em hectolitro basta multiplicar por 10"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 3:
                    input_capacidade_2.value = valor * 100;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar quilolitro em decalitro basta multiplicar por 100"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 4:
                    input_capacidade_2.value = valor * 1000;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar quilolitro em litro basta multiplicar por 1.000"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 5:
                    input_capacidade_2.value = valor * 10000;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar quilolitro em decilitro basta multiplicar por 10.000"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 6:
                    input_capacidade_2.value = valor * 100000;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar quilolitro em centilitro basta multiplicar por 100.000"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 7:
                    input_capacidade_2.value = valor * 1000000;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar quilolitro em mililitro basta multiplicar por 1.000.000"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
            }
            break; //FIM KM

        case 2: //INICIO HECTOMETRO
            switch (select2) {
                case 1:
                    input_capacidade_2.value = valor / 10;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar hectolitro em quilolitro basta dividir por 10"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 2:
                    input_capacidade_2.value = valor;
                    break;
                case 3:
                    input_capacidade_2.value = valor * 10;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar hectolitro em decalitro basta multiplicar por 10"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 4:
                    input_capacidade_2.value = valor * 100;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar hectolitro em litro basta multiplicar por 100"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 5:
                    input_capacidade_2.value = valor * 1000;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar hectolitro em decilitro basta multiplicar por 1.000"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 6:
                    input_capacidade_2.value = valor * 10000;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar hectolitro em centilitro basta multiplicar por 10.000"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 7:
                    input_capacidade_2.value = valor * 100000;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar hectolitro em mililitro basta multiplicar por 10.000"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
            }
            break; //FIM HECTOMETRO

        case 3: //INICIO DECAMETRO
            switch (select2) {
                case 1:
                    input_capacidade_2.value = valor / 100;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decalitro em quilolitro basta dividir por 100"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 2:
                    input_capacidade_2.value = valor / 10;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decalitro em hectolitro basta dividir por 10"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 3:
                    input_capacidade_2.value = valor;
                    break;
                case 4:
                    input_capacidade_2.value = valor * 10;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decalitro em litro basta multiplicar por 10"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 5:
                    input_capacidade_2.value = valor * 100;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decalitro em decilitro basta multiplicar por 100"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 6:
                    input_capacidade_2.value = valor * 1000;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decalitro em centrilitro basta multiplicar por 1.000"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 7:
                    input_capacidade_2.value = valor * 10000;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decalitro em mililitro basta multiplicar por 10.000"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
            }
            break; //FIM DECAMETRO

        case 4: //INICIO METRO
            switch (select2) {
                case 1:
                    input_capacidade_2.value = valor / 1000;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar litro em quilolitro basta dividir por 1.000"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 2:
                    input_capacidade_2.value = valor / 100;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar litro em hectolitro basta dividir por 100"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 3:
                    input_capacidade_2.value = valor / 10;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar litro em decalitro basta dividir por 10"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 4:
                    input_capacidade_2.value = valor;
                    break;
                case 5:
                    input_capacidade_2.value = valor * 10;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar litro em decilitro basta multiplicar por 10"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 6:
                    input_capacidade_2.value = valor * 100;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar litro em centilitro basta multiplicar por 100"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 7:
                    input_capacidade_2.value = valor * 1000;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar litro em mililitro basta multiplicar por 1.000"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
            }
            break; //FIM METRO

        case 5: //INICIO DECIMETRO
            switch (select2) {
                case 1:
                    input_capacidade_2.value = valor / 10000;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decilitro em quilolitro basta dividir por 10.000"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 2:
                    input_capacidade_2.value = valor / 1000;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decilitro em hectolitro basta dividir por 1.000"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 3:
                    input_capacidade_2.value = valor / 100;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decilitro em decalitro basta dividir por 100"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 4:
                    input_capacidade_2.value = valor / 10;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decilitro em litro basta dividir por 10"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 5:
                    input_capacidade_2.value = valor;
                    break;
                case 6:
                    input_capacidade_2.value = valor * 10;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decilitro em centilitro basta multiplicar por 10"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 7:
                    input_capacidade_2.value = valor * 100;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decilitro em mililitro basta multiplicar por 100"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
            }
            break; //FIM DECIMETRO

        case 6: //INICIO CENTIMETRO
            switch (select2) {
                case 1:
                    input_capacidade_2.value = valor / 100000;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar centilitro em quilolitro basta dividir por 100.000"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 2:
                    input_capacidade_2.value = valor / 10000;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar centilitro em hectolitro basta dividir por 10.000"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 3:
                    input_capacidade_2.value = valor / 1000;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar centilitro em decalitro basta dividir por 1.000"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 4:
                    input_capacidade_2.value = valor / 100;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar centilitro em litro basta dividir por 100"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 5:
                    input_capacidade_2.value = valor / 10;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar centilitro em decilitro basta dividir por 10"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 6:
                    input_capacidade_2.value = valor;
                    break;
                case 7:
                    input_capacidade_2.value = valor * 10;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar centilitro em decilitro basta multiplicar por 10"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
            }
            break; //FIM CENTIMETRO
        case 7: //INICIO MILIMETRO
            switch (select2) {
                case 1:
                    input_capacidade_2.value = valor / 1000000;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar mililitro em quilolitro basta dividir por 1.000.000"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 2:
                    input_capacidade_2.value = valor / 100000;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar mililitro em hectolitro basta dividir por 100.000"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 3:
                    input_capacidade_2.value = valor / 10000;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar mililitro em decalitro basta dividir por 10.000"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 4:
                    input_capacidade_2.value = valor / 1000;
                    capacidade.innerHTML = "";

                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar mililitro em litro basta dividir por 1.000"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 5:
                    input_capacidade_2.value = valor / 100;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar mililitro em decilitro basta dividir por 100"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 6:
                    input_capacidade_2.value = valor / 10;
                    capacidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar mililitro em centilitro basta dividir por 10"
                    );
                    p.appendChild(texto);
                    capacidade.appendChild(p);
                    break;
                case 7:
                    input_capacidade_2.value = valor;
                    break;
            }
            break; //FIM MILIMETRO
    }
}