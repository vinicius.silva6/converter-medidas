var select_velocidade = document.querySelector("#velocidade1");
var select_velocidade_2 = document.querySelector("#velocidade2");
var input1 = document.querySelector("#input1");
var input2 = document.querySelector("#input2");
var comprimento = document.querySelector("#comprimento");

select_valor2 = select_velocidade_2.value;
select_valor1 = select_velocidade.value;
select_valor1 = parseInt(select_valor1);
select_valor2 = parseInt(select_valor2);

var p = document.createElement("p");

select_velocidade.addEventListener("change", function() {
    select_valor1 = this.value;
    select_valor1 = parseInt(select_valor1);
    converter_comprimento(valor_input1, select_valor1, select_valor2);
    return select_valor1;
});

select_velocidade_2.addEventListener("change", function() {
    select_valor2 = this.value;
    select_valor2 = parseInt(select_valor2);
    converter_comprimento(valor_input1, select_valor1, select_valor2);
    return select_valor2;
});

input1.addEventListener("keyup", function() {
    if (this.value == "") {
        input2.value = "";
    } else {
        valor_input1 = this.value;
        valor_input1 = valor_input1.replace(/,/i, ".");
        console.log(valor_input1);
        valor_input1 = parseFloat(valor_input1);
        converter_comprimento(valor_input1, select_valor1, select_valor2);
    }
    return valor_input1;
});

function converter_comprimento(valor, select1, select2) {
    switch (select1) {
        case 1: //INICIO KM
            switch (select2) {
                case 1:
                    input2.value = valor;
                    comprimento.innerHTML = "";
                    break;
                case 2:
                    input2.value = valor * 10;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar quilometro em hectometro basta multiplicar por 10"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 3:
                    input2.value = valor * 100;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar quilometro em decametro basta multiplicar por 100"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 4:
                    input2.value = valor * 1000;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar quilometro em metro basta multiplicar por 1.000"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 5:
                    input2.value = valor * 10000;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar quilometro em decimetro basta multiplicar por 10.000"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 6:
                    input2.value = valor * 100000;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar quilometro em centimetro basta multiplicar por 100.000"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 7:
                    input2.value = valor * 1000000;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar quilometro em milimetro basta multiplicar por 1.000.000"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
            }
            break; //FIM KM

        case 2: //INICIO HECTOMETRO
            switch (select2) {
                case 1:
                    input2.value = valor / 10;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar hectometro em quilometro basta dividir por 10"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 2:
                    input2.value = valor;
                    break;
                case 3:
                    input2.value = valor * 10;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar hectometro em decametro basta multiplicar por 10"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 4:
                    input2.value = valor * 100;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar hectometro em metro basta multiplicar por 100"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 5:
                    input2.value = valor * 1000;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar hectometro em decimetro basta multiplicar por 1.000"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 6:
                    input2.value = valor * 10000;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar hectometro em centimetro basta multiplicar por 10.000"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 7:
                    input2.value = valor * 100000;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar hectometro em milimetro basta multiplicar por 10.000"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
            }
            break; //FIM HECTOMETRO

        case 3: //INICIO DECAMETRO
            switch (select2) {
                case 1:
                    input2.value = valor / 100;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decametro em quilometro basta dividir por 100"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 2:
                    input2.value = valor / 10;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decametro em hectometro basta dividir por 10"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 3:
                    input2.value = valor;
                    break;
                case 4:
                    input2.value = valor * 10;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decametro em metro basta multiplicar por 10"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 5:
                    input2.value = valor * 100;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decametro em decimetro basta multiplicar por 100"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 6:
                    input2.value = valor * 1000;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decametro em centimetro basta multiplicar por 1.000"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 7:
                    input2.value = valor * 10000;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decametro em milimetro basta multiplicar por 10.000"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
            }
            break; //FIM DECAMETRO

        case 4: //INICIO METRO
            switch (select2) {
                case 1:
                    input2.value = valor / 1000;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar metro em quilometro basta dividir por 1.000"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 2:
                    input2.value = valor / 100;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar metro em hectometro basta dividir por 100"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 3:
                    input2.value = valor / 10;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar metro em decametro basta dividir por 10"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 4:
                    input2.value = valor;
                    break;
                case 5:
                    input2.value = valor * 10;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar metro em decimetro basta multiplicar por 10"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 6:
                    input2.value = valor * 100;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar metro em centimetro basta multiplicar por 100"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 7:
                    input2.value = valor * 1000;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar metro em milimetro basta multiplicar por 1.000"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
            }
            break; //FIM METRO

        case 5: //INICIO DECIMETRO
            switch (select2) {
                case 1:
                    input2.value = valor / 10000;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decimetro em quilometro basta dividir por 10.000"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 2:
                    input2.value = valor / 1000;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decimetro em hectometro basta dividir por 1.000"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 3:
                    input2.value = valor / 100;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decimetro em decametro basta dividir por 100"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 4:
                    input2.value = valor / 10;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decimetro em metro basta dividir por 10"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 5:
                    input2.value = valor;
                    break;
                case 6:
                    input2.value = valor * 10;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decimetro em centimetro basta multiplicar por 10"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 7:
                    input2.value = valor * 100;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decimetro em milimetro basta multiplicar por 100"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
            }
            break; //FIM DECIMETRO

        case 6: //INICIO CENTIMETRO
            switch (select2) {
                case 1:
                    input2.value = valor / 100000;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar centimetro em quilometro basta dividir por 100.000"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 2:
                    input2.value = valor / 10000;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar centimetro em hectometro basta dividir por 10.000"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 3:
                    input2.value = valor / 1000;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar centimetro em decametro basta dividir por 1.000"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 4:
                    input2.value = valor / 100;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar centimetro em metro basta dividir por 100"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 5:
                    input2.value = valor / 10;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar centimetro em decimetro basta dividir por 10"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 6:
                    input2.value = valor;
                    break;
                case 7:
                    input2.value = valor * 10;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar centimetro em milimetro basta multiplicar por 10"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
            }
            break; //FIM CENTIMETRO
        case 7: //INICIO MILIMETRO
            switch (select2) {
                case 1:
                    input2.value = valor / 1000000;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar milimetro em quilometro basta dividir por 1.000.000"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 2:
                    input2.value = valor / 100000;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar milimetro em hectometro basta dividir por 100.000"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 3:
                    input2.value = valor / 10000;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar milimetro em decametro basta dividir por 10.000"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 4:
                    input2.value = valor / 1000;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar milimetro em metro basta dividir por 1.000"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 5:
                    input2.value = valor / 100;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar milimetro em decimetro basta dividir por 100"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 6:
                    input2.value = valor / 10;
                    comprimento.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar milimetro em centimetro basta dividir por 10"
                    );
                    p.appendChild(texto);
                    comprimento.appendChild(p);
                    break;
                case 7:
                    input2.value = valor;
                    break;
            }
            break; //FIM MILIMETRO
    }
}