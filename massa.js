var select_massa = document.querySelector("#select_massa");
var select_massa_2 = document.querySelector("#select_massa_2");
var input_massa1 = document.querySelector("#input_massa1");
var input_massa_2 = document.querySelector("#input_massa2");
var massa = document.querySelector("#massa");

select_valor_massa2 = select_massa_2.value;
select_valor_massa1 = select_massa.value;
select_valor_massa1 = parseInt(select_valor_massa1);
select_valor_massa2 = parseInt(select_valor_massa2);
var p = document.createElement("p");

select_massa.addEventListener("change", function() {
    select_valor_massa1 = this.value;
    select_valor_massa1 = parseInt(select_valor_massa1);
    converter_massa(valor_input_1, select_valor_massa1, select_valor_massa2);
    return select_valor_massa1;
});

select_massa_2.addEventListener("change", function() {
    select_valor_massa2 = this.value;
    select_valor_massa2 = parseInt(select_valor_massa2);
    converter_massa(valor_input_1, select_valor_massa1, select_valor_massa2);
    return select_valor_massa2;
});

input_massa1.addEventListener("keyup", function() {
    if (this.value == "") {
        input2.value = "";
    } else {
        valor_input_1 = this.value;
        valor_input_1 = valor_input_1.replace(/,/i, ".");
        console.log(valor_input_1);
        valor_input_1 = parseFloat(valor_input_1);
        converter_massa(valor_input_1, select_valor_massa1, select_valor_massa2);
    }
    return valor_input_1;
});

function converter_massa(valor, select1, select2) {
    switch (select1) {
        case 1: //INICIO KM
            switch (select2) {
                case 1:
                    input_massa_2.value = valor;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    break;
                case 2:
                    input_massa_2.value = valor * 10;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar quilograma em hectograma basta multiplicar por 10"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 3:
                    input_massa_2.value = valor * 100;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar quilograma em decagrama basta multiplicar por 100"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 4:
                    input_massa_2.value = valor * 1000;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar quilograma em grama basta multiplicar por 1.000"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 5:
                    input_massa_2.value = valor * 10000;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar quilograma em decigrama basta multiplicar por 10.000"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 6:
                    input_massa_2.value = valor * 100000;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar quilograma em centigrama basta multiplicar por 100.000"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 7:
                    input_massa_2.value = valor * 1000000;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar quilograma em miligrama basta multiplicar por 1.000.000"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
            }
            break; //FIM KM

        case 2: //INICIO HECTOMETRO
            switch (select2) {
                case 1:
                    input_massa_2.value = valor / 10;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar hectograma em kilograma basta dividir por 10"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 2:
                    input_massa_2.value = valor;
                    break;
                case 3:
                    input_massa_2.value = valor * 10;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar hectograma em decagrama basta multiplicar por 10"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 4:
                    input_massa_2.value = valor * 100;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar hectograma em grama basta multiplicar por 100"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 5:
                    input_massa_2.value = valor * 1000;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar hectograma em decigrama basta multiplicar por 1.000"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 6:
                    input_massa_2.value = valor * 10000;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar hectograma em centigrama basta multiplicar por 10.000"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 7:
                    input_massa_2.value = valor * 100000;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar hectograma em miligrama basta multiplicar por 10.000"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
            }
            break; //FIM HECTOMETRO

        case 3: //INICIO DECAMETRO
            switch (select2) {
                case 1:
                    input_massa_2.value = valor / 100;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decagrama em quilograma basta dividir por 100"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 2:
                    input_massa_2.value = valor / 10;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decagrama em hectograma basta dividir por 10"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 3:
                    input_massa_2.value = valor;
                    break;
                case 4:
                    input_massa_2.value = valor * 10;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decagrama em grama basta multiplicar por 10"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 5:
                    input_massa_2.value = valor * 100;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decagrama em decigrama basta multiplicar por 100"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 6:
                    input_massa_2.value = valor * 1000;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decagrama em centrigrama basta multiplicar por 1.000"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 7:
                    input_massa_2.value = valor * 10000;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decagrama em miligrama basta multiplicar por 10.000"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
            }
            break; //FIM DECAMETRO

        case 4: //INICIO METRO
            switch (select2) {
                case 1:
                    input_massa_2.value = valor / 1000;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar grama em kilograma basta dividir por 1.000"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 2:
                    input_massa_2.value = valor / 100;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar grama em hectograma basta dividir por 100"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 3:
                    input_massa_2.value = valor / 10;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar grama em decagrama basta dividir por 10"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 4:
                    input_massa_2.value = valor;
                    break;
                case 5:
                    input_massa_2.value = valor * 10;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar grama em decigrama basta multiplicar por 10"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 6:
                    input_massa_2.value = valor * 100;
                    massa.innerHTML = "";

                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar grama em centigrama basta multiplicar por 100"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 7:
                    input_massa_2.value = valor * 1000;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar grama em miligrama basta multiplicar por 1.000"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
            }
            break; //FIM METRO

        case 5: //INICIO DECIMETRO
            switch (select2) {
                case 1:
                    input_massa_2.value = valor / 10000;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decigrama em quilograma basta dividir por 10.000"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 2:
                    input_massa_2.value = valor / 1000;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decigrama em hectograma basta dividir por 1.000"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 3:
                    input_massa_2.value = valor / 100;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decigrama em decagrama basta dividir por 100"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 4:
                    input_massa_2.value = valor / 10;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decigrama em grama basta dividir por 10"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 5:
                    input_massa_2.value = valor;
                    break;
                case 6:
                    input_massa_2.value = valor * 10;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decigrama em centigrama basta multiplicar por 10"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 7:
                    input_massa_2.value = valor * 100;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar decigrama em miligrama basta multiplicar por 100"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
            }
            break; //FIM DECIMETRO

        case 6: //INICIO CENTIMETRO
            switch (select2) {
                case 1:
                    input_massa_2.value = valor / 100000;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar centigrama em quilograma basta dividir por 100.000"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 2:
                    input_massa_2.value = valor / 10000;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar centigrama em hectograma basta dividir por 10.000"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 3:
                    input_massa_2.value = valor / 1000;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar centigrama em decagrama basta dividir por 1.000"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 4:
                    input_massa_2.value = valor / 100;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar centigrama em grama basta dividir por 100"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 5:
                    input_massa_2.value = valor / 10;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar centigrama em decigrama basta dividir por 10"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 6:
                    input_massa_2.value = valor;
                    break;
                case 7:
                    input_massa_2.value = valor * 10;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar centigrama em decigrama basta multiplicar por 10"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
            }
            break; //FIM CENTIMETRO
        case 7: //INICIO MILIMETRO
            switch (select2) {
                case 1:
                    input_massa_2.value = valor / 1000000;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar miligrama em quilograma basta dividir por 1.000.000"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 2:
                    input_massa_2.value = valor / 100000;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar miligrama em hectograma basta dividir por 100.000"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 3:
                    input_massa_2.value = valor / 10000;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar miligrama em decagrama basta dividir por 10.000"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 4:
                    input_massa_2.value = valor / 1000;
                    massa.innerHTML = "";

                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar miligrama em grama basta dividir por 1.000"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 5:
                    input_massa_2.value = valor / 100;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar miligrama em decigrama basta dividir por 100"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 6:
                    input_massa_2.value = valor / 10;
                    massa.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para transformar miligrama em centigrama basta dividir por 10"
                    );
                    p.appendChild(texto);
                    massa.appendChild(p);
                    break;
                case 7:
                    input_massa_2.value = valor;
                    break;
            }
            break; //FIM MILIMETRO
    }
}