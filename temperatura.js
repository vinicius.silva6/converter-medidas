var select_temperatura = document.querySelector("#select_temperatura");
var select_temperatura_2 = document.querySelector("#select_temperatura_2");
var input_temperatura1 = document.querySelector("#input_temperatura1");
var input_temperatura_2 = document.querySelector("#input_temperatura2");
var temperatura = document.querySelector("#temperatura");

select_valor_temperatura2 = select_temperatura_2.value;
select_select_temperatura = select_temperatura.value;
select_select_temperatura = parseInt(select_select_temperatura);
select_valor_temperatura2 = parseInt(select_valor_temperatura2);

var p = document.createElement("p");

select_temperatura.addEventListener("change", function() {
    select_select_temperatura = this.value;
    select_select_temperatura = parseInt(select_select_temperatura);
    converter_temperatura(
        valor_input_temperatura1,
        select_select_temperatura,
        select_valor_temperatura2
    );
    return select_select_temperatura;
});

select_temperatura_2.addEventListener("change", function() {
    select_valor_temperatura2 = this.value;
    select_valor_temperatura2 = parseInt(select_valor_temperatura2);
    converter_temperatura(
        valor_input_temperatura1,
        select_select_temperatura,
        select_valor_temperatura2
    );
    return select_valor_temperatura2;
});

input_temperatura1.addEventListener("keyup", function() {
    if (this.value == "") {
        input_temperatura_2.value = "";
    } else {
        valor_input_temperatura1 = this.value;
        valor_input_temperatura1 = valor_input_temperatura1.replace(/,/i, ".");
        valor_input_temperatura1 = parseFloat(valor_input_temperatura1);
        console.log(select_select_temperatura, select_valor_temperatura2);
        converter_temperatura(
            valor_input_temperatura1,
            select_select_temperatura,
            select_valor_temperatura2
        );
    }
    return valor_input_temperatura1;
});

function converter_temperatura(valor, select1, select2) {
    switch (select1) {
        case 1:
            switch (select2) {
                case 1:
                    input_temperatura_2.value = valor;
                    temperatura.innerHTML = "";
                    break;
                case 2:
                    input_temperatura_2.value = valor * (9 / 5) + 32;
                    temperatura.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de graus para fahrenheit, use a formula -> (0 °C × 9/5) + 32"
                    );
                    p.appendChild(texto);
                    temperatura.appendChild(p);
                    break;
                case 3:
                    input_temperatura_2.value = valor + 273.15;
                    temperatura.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de graus para kelvin, use a formula -> 0 °C + 273,15"
                    );
                    p.appendChild(texto);
                    temperatura.appendChild(p);
                    break;
            }
            break;
        case 2:
            switch (select2) {
                case 1:
                    input_temperatura_2.value = (valor - 32) * (5 / 9);
                    temperatura.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de fahrenheit para graus, use a formula -> (0 °F − 32) × 5/9 "
                    );
                    p.appendChild(texto);
                    temperatura.appendChild(p);
                    break;

                case 2:
                    input_temperatura_2.value = valor;
                    temperatura.innerHTML = "";
                    break;
                case 3:
                    input_temperatura_2.value = (valor - 32) * (5 / 9) + 273.15;
                    temperatura.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de fahrenheit para graus, use a formula ->(0 °F − 32) × 5/9 + 273,15 "
                    );
                    p.appendChild(texto);
                    temperatura.appendChild(p);
                    break;
            }
            break;
        case 3:
            switch (select2) {
                case 1:
                    input_temperatura_2.value = valor - 273.15;
                    temperatura.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de kelvin para graus , use a formula -> 0 K − 273,15"
                    );
                    p.appendChild(texto);
                    temperatura.appendChild(p);
                    break;

                case 2:
                    input_temperatura_2.value = ((0 - 273.15) * (9 / 5) + 32).toFixed(2);
                    temperatura.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de kelvin para fahrenheit, use a formula ->(0 °F − 32) × 5/9 + 273,15 "
                    );
                    p.appendChild(texto);
                    temperatura.appendChild(p);
                    break;

                case 3:
                    input_temperatura_2.value = valor;
                    temperatura.innerHTML = "";
                    break;
            }
            break;
    }
}