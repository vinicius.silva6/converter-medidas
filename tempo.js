var select_tempo = document.querySelector("#select_tempo");
var select_tempo_2 = document.querySelector("#select_tempo_2");
var input_tempo1 = document.querySelector("#input_tempo1");
var input_tempo_2 = document.querySelector("#input_tempo2");
var tempo = document.querySelector("#tempo");

select_valor_tempo2 = select_tempo_2.value;
select_valor_tempo1 = select_tempo.value;
select_valor_tempo1 = parseInt(select_valor_tempo1);
select_valor_tempo2 = parseInt(select_valor_tempo2);
var p = document.createElement("p");
select_tempo.addEventListener("change", function() {
    select_valor_tempo1 = this.value;
    select_valor_tempo1 = parseInt(select_valor_tempo1);
    converter_tempo(valor_input_tempo1, select_valor_tempo1, select_valor_tempo2);
    return select_valor_tempo1;
});

select_tempo_2.addEventListener("change", function() {
    select_valor_tempo2 = this.value;
    select_valor_tempo2 = parseInt(select_valor_tempo2);
    converter_tempo(valor_input_tempo1, select_valor_tempo1, select_valor_tempo2);
    return select_valor_tempo2;
});

input_tempo1.addEventListener("keyup", function() {
    valor_input_tempo1 = this.value;
    valor_input_tempo1 = valor_input_tempo1.replace(/,/i, ".");
    valor_input_tempo1 = parseFloat(valor_input_tempo1);
    if (valor_input_tempo1 == "") {
        input_tempo_2.value = "";
        console.log("funciona");
    } else {
        converter_tempo(
            valor_input_tempo1,
            select_valor_tempo1,
            select_valor_tempo2
        );
    }
    return valor_input_tempo1;
});

function converter_tempo(valor, select1, select2) {
    switch (select1) {
        case 1: //NANO SEGUNDO
            switch (select2) {
                case 1:
                    input_tempo_2.value = valor;
                    tempo.innerHTML = "";
                    break;
                case 2:
                    input_tempo_2.value = valor / 1000;
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de nanosegundo para microsegundo divida o tempo por 1000"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
                case 3:
                    input_tempo_2.value = valor / (1 * Math.pow(10, 6));
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de nanosegundo para milisegundo divida o tempo por 1e+6"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
                case 4:
                    input_tempo_2.value = valor / (1 * Math.pow(10, 9));
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de nanosegundo para segundo divida o tempo por 1e+9"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
                case 5:
                    input_tempo_2.value = valor / (6 * Math.pow(10, 10));
                    var conversao = valor / (6 * Math.pow(10, 10));
                    console.log(Math.fround(conversao).toFixed(2));
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de nanosegundo para minuto divida o tempo por 6e+10"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
                case 6:
                    input_tempo_2.value = valor / (3.6 * Math.pow(10, 12));
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de nanosegundo para hora divida o tempo por 3,6e+12"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
                case 7:
                    input_tempo_2.value = valor / (8.64 * Math.pow(10, 13));
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de nanosegundo para dia divida o tempo por 8,64e+13"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
            }
            break;
        case 2:
            switch (select_valor_tempo2) {
                case 1:
                    input_tempo_2.value = valor * 1000;
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de microsegundo para nanosegundo multiplique o tempo por 1000"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
                case 2:
                    input_tempo_2.value = valor;
                    tempo.innerHTML = "";
                case 3:
                    input_tempo_2.value = valor / 1000;
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de microsegundo para milisegundo divida o tempo por 1000"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
                case 4:
                    input_tempo_2.value = valor / (1 * Math.pow(10, 6));
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de microsegundo para segundo divida o tempo por 1e+6"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
                case 5:
                    input_tempo_2.value = valor / (6 * Math.pow(10, 7));
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de microsegundo para minuto divida o tempo por 6e+7"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
                case 6:
                    input_tempo_2.value = (valor / (3.6 * Math.pow(10, 9))).toFixed(2);
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de microsegundo para hora divida o tempo por 3,6e+9"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
                case 7:
                    input_tempo_2.value = valor / (8.6 * Math.pow(10, 10));
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de microsegundo para dia divida o tempo por 8,6e+10"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
            }
            break;
        case 3:
            switch (select2) {
                case 1:
                    input_tempo_2.value = valor * (1 * Math.pow(10, 6));
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de milisegundo para nanosegundo multiplique o tempo por 1e+6"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
                case 2:
                    input_tempo_2.value = valor * 1000;
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de milisegundo para microsegundo multiplique o tempo por 1000"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
                case 3:
                    input_tempo_2.value = valor;
                    tempo.innerHTML = "";
                    break;
                case 4:
                    input_tempo_2.value = valor / 1000;
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de milisegundo para segundo divida o tempo por 1000"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
                case 5:
                    input_tempo_2.value = valor / 60000;
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de milisegundo para minuto divida o tempo por 60000"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
                case 6:
                    input_tempo_2.value = valor / (3.6 * Math.pow(10, 6));
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de milisegundo para hora divida o tempo por 3,6e+6"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;

                case 7:
                    input_tempo_2.value = valor / (8.64 * Math.pow(10, 7));
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de milisegundo para dia divida o tempo por 8,64e+7"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
            }
            break;
        case 4:
            switch (select2) {
                case 1:
                    input_tempo_2.value = valor * (1 * Math.pow(10, 9));
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de segundo para nanosegundo multiplique o tempo por 1e+9"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
                case 2:
                    input_tempo_2.value = valor * (1 * Math.pow(10, 6));
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de segundo para microsegundo multiplique o tempo por 1e+6"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;

                case 3:
                    input_tempo_2.value = valor * 1000;
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de segundo para milisegundo multiplique o tempo por 1000"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
                case 4:
                    input_tempo_2.value = valor;
                    tempo.innerHTML = "";
                    break;
                case 5:
                    input_tempo_2.value = valor / 60;
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de segundo para minuto divida o tempo por 60"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
                case 6:
                    input_tempo_2.value = (valor / 3600).toFixed(2);
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de segundo para hora divida o tempo por 3600"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
                case 7:
                    input_tempo_2.value = valor / 86400;
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de segundo para dia divida o tempo por 86400"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
            }
            break;
        case 5:
            switch (select2) {
                case 1:
                    input_tempo_2.value = valor * (6 * Math.pow(10, 10));
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de minuto para nanosegundo multiplique o tempo por 6e+10"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
                case 2:
                    input_tempo_2.value = valor * (6 * Math.pow(10, 7));
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de minuto para microsegundo multiplique o tempo por 6e+7"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
                case 3:
                    input_tempo_2.value = valor * 60000;
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de minuto para milisegundo multiplique o tempo por 60000"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
                case 4:
                    input_tempo_2.value = valor * 60;
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de minuto para segundo multiplique o tempo por 60"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
                case 5:
                    input_tempo_2.value = valor;
                    tempo.innerHTML = "";
                    break;
                case 6:
                    input_tempo_2.value = valor / 60;
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de minuto para hora divida o tempo por 60"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;

                case 7:
                    input_tempo_2.value = valor / 1440;
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de minuto para dia divida o tempo por 1440"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
            }
            break;
        case 6:
            switch (select2) {
                case 1:
                    input_tempo_2.value = valor * (3.6 * Math.pow(10, 12));
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de minuto para nanosegundo multiplique o tempo por 3,6e+12"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
                case 2:
                    input_tempo_2.value = valor * (3.6 * Math.pow(10, 9));
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de minuto para microsegundo multiplique o tempo por 3,6e+9"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;

                case 3:
                    input_tempo_2.value = valor * (3.6 * Math.pow(10, 6));
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de minuto para milisegundo multiplique o tempo por 3,6e+6"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
                case 4:
                    input_tempo_2.value = valor * 3600;
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de hora para segundo multiplique o tempo por 3600"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
                case 5:
                    input_tempo_2.value = valor * 60;
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de hora para minuto multiplique o tempo por 60"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
                case 6:
                    input_tempo_2.value = valor;
                    tempo.innerHTML = "";
                    break;
                case 7:
                    input_tempo_2.value = valor / 24;
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de hora para dia divida o tempo por 24"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
            }
            break;
        case 7:
            switch (select2) {
                case 1:
                    input_tempo_2.value = valor * (8.64 * Math.pow(10, 13));
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de minuto para nanosegundo multiplique o tempo por 8,64e+13"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
                case 2:
                    input_tempo_2.value = valor * (8.64 * Math.pow(10, 10));
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de minuto para microsegundo multiplique o tempo por 8,64e+10"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
                case 3:
                    input_tempo_2.value = valor * (8.64 * Math.pow(10, 7));
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de dia para milisegundo multiplique o tempo por 8,64e+7"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
                case 4:
                    input_tempo_2.value = valor * 86400;
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de dia para minuto multiplique o tempo por 86400"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
                case 5:
                    input_tempo_2.value = valor * 1440;
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de dia para minuto multiplique o tempo por 1440"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
                case 6:
                    input_tempo_2.value = valor * 24;
                    tempo.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de dia para hora multiplique o tempo por 24"
                    );
                    p.appendChild(texto);
                    tempo.appendChild(p);
                    break;
                case 7:
                    input_tempo_2.value = valor;
                    tempo.innerHTML = "";
                    break;
            }
            break;
    }
}