var select_velocidade = document.querySelector("#select_velocidade");
var select_velocidade_2 = document.querySelector("#select_velocidade_2");
var input_velocidade1 = document.querySelector("#input_velocidade1");
var input_velocidade_2 = document.querySelector("#input_velocidade2");
var velocidade = document.querySelector("#velocidade");

select_valor_velocidade2 = select_velocidade_2.value;
select_valor_velocidade1 = select_velocidade.value;
select_valor_velocidade1 = parseInt(select_valor_velocidade1);
select_valor_velocidade2 = parseInt(select_valor_velocidade2);
var p = document.createElement("p");

select_velocidade.addEventListener("change", function() {
    select_valor_velocidade1 = this.value;
    select_valor_velocidade1 = parseInt(select_valor_velocidade1);
    converter_velocidade(
        valor_input_velocidade1,
        select_valor_velocidade1,
        select_valor_velocidade2
    );
    return select_valor_velocidade1;
});

select_velocidade_2.addEventListener("change", function() {
    select_valor_velocidade2 = this.value;
    select_valor_velocidade2 = parseInt(select_valor_velocidade2);
    converter_velocidade(
        valor_input_velocidade1,
        select_valor_velocidade1,
        select_valor_velocidade2
    );
    return select_valor_velocidade2;
});

input_velocidade1.addEventListener("keyup", function() {
    if (this.value == "") {
        input_velocidade_2.value = "";
    } else {
        valor_input_velocidade1 = this.value;
        valor_input_velocidade1 = valor_input_velocidade1.replace(/,/i, ".");

        valor_input_velocidade1 = parseFloat(valor_input_velocidade1);
        converter_velocidade(
            valor_input_velocidade1,
            select_valor_velocidade1,
            select_valor_velocidade2
        );
    }
    return valor_input_velocidade1;
});

function converter_velocidade(valor, select1, select2) {
    switch (select1) {
        case 1:
            switch (select2) {
                case 1:
                    input_velocidade_2.value = valor;
                    velocidade.innerHTML = "";
                    break;
                case 2:
                    input_velocidade_2.value = (valor * 1.467).toFixed(4);
                    velocidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para obter um resultado aproximado, basta multiplicar por 1,467"
                    );
                    p.appendChild(texto);
                    velocidade.appendChild(p);

                    break;

                case 3:
                    input_velocidade_2.value = (valor / 2.237).toFixed(4);
                    velocidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter, basta dividir o valor por 2,237"
                    );
                    p.appendChild(texto);
                    velocidade.appendChild(p);
                    break;

                case 4:
                    input_velocidade_2.value = (valor * 1.609).toFixed(4);
                    velocidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para obter um resultado aproximado, basta multiplicar por 1,609"
                    );
                    p.appendChild(texto);
                    velocidade.appendChild(p);
                    break;
                case 5:
                    input_velocidade_2.value = (valor / 1.151).toFixed(4);
                    velocidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para obter um resultado aproximado, basta multiplicar por 1.151"
                    );
                    p.appendChild(texto);
                    velocidade.appendChild(p);
                    break;
            }
        case 2:
            switch (select2) {
                case 1:
                    input_velocidade_2.value = (valor / 1.467).toFixed(4);
                    velocidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para obter um resultado aproximado, basta dividir o valor por 1.467"
                    );
                    p.appendChild(texto);
                    velocidade.appendChild(p);
                    break;
                case 2:
                    input_velocidade_2.value = valor;
                    velocidade.innerHTML = "";
                    break;
                case 3:
                    input_velocidade_2.value = (valor / 3.281).toFixed(4);
                    velocidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para obter um resultado aproximado, basta dividir o valor por 3,281"
                    );
                    p.appendChild(texto);
                    velocidade.appendChild(p);
                    break;

                case 4:
                    input_velocidade_2.value = valor * 1.097;
                    velocidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para obter um resultado aproximado, basta multiplicar o valor por 1,097"
                    );
                    p.appendChild(texto);
                    velocidade.appendChild(p);
                    break;

                case 5:
                    input_velocidade_2.value = (valor / 1.688).toFixed(4);
                    velocidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para obter um resultado aproximado, basta dividir o valor por 1.688"
                    );
                    p.appendChild(texto);
                    velocidade.appendChild(p);
                    break;
            }
        case 3:
            switch (select2) {
                case 1:
                    input_velocidade_2.value = valor * 2.237;
                    velocidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de metro/segundo para milhas/hora, multiplique o valor por 2,237"
                    );
                    p.appendChild(texto);
                    velocidade.appendChild(p);
                    break;

                case 2:
                    input_velocidade_2.value = valor * 3.281;
                    velocidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para obter um resultado aproximado, basta multiplicar o valor por 3.281"
                    );
                    p.appendChild(texto);
                    velocidade.appendChild(p);
                    break;

                case 3:
                    input_velocidade_2.value = valor;
                    velocidade.innerHTML = "";
                    break;
                case 4:
                    input_velocidade_2.value = valor * 3.6;
                    velocidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de metro/segundo para kilometro/hora, multiplique o valor por 3.6"
                    );
                    p.appendChild(texto);
                    velocidade.appendChild(p);
                    break;
                case 5:
                    input_velocidade_2.value = valor * 1.944;
                    velocidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para obter um resultado aproximado, basta multiplicar o valor por 1.944"
                    );
                    p.appendChild(texto);
                    velocidade.appendChild(p);
                    break;
            }
            break;
        case 4:
            switch (select2) {
                case 1:
                    input_velocidade_2.value = (valor / 1.609).toFixed(4);
                    velocidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para obter um resultado aproximado, basta dividir o valor 1,609"
                    );
                    p.appendChild(texto);
                    velocidade.appendChild(p);
                    break;
                case 2:
                    input_velocidade_2.value = (valor / 1.097).toFixed(4);
                    velocidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para obter um resultado aproximado, basta dividir o valor 1,097"
                    );
                    p.appendChild(texto);
                    velocidade.appendChild(p);
                    break;
                case 3:
                    input_velocidade_2.value = valor / 3.6;
                    velocidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de  kilometro/hora para metro/segundo, divida o valor por 3.6"
                    );
                    p.appendChild(texto);
                    velocidade.appendChild(p);
                    break;

                case 4:
                    input_velocidade_2.value = valor;
                    velocidade.innerHTML = "";
                    break;
                case 5:
                    input_velocidade_2.value = (valor / 1.852).toFixed(4);
                    velocidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de  kilometro/hora para nó, divida o valor por 1.852"
                    );
                    p.appendChild(texto);
                    velocidade.appendChild(p);
                    break;
            }
            break;
        case 5:
            switch (select2) {
                case 1:
                    input_velocidade_2.value = valor * 1.151;
                    velocidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para obter um resultado aproximado, basta mulitplicar o valor 1,151"
                    );
                    p.appendChild(texto);
                    velocidade.appendChild(p);
                    break;

                case 2:
                    input_velocidade_2.value = valor * 1.688;
                    velocidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para obter um resultado aproximado, basta mulitplicar o valor 1,688"
                    );
                    p.appendChild(texto);
                    velocidade.appendChild(p);
                    break;
                case 3:
                    input_velocidade_2.value = (valor / 1.944).toFixed(4);
                    velocidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para obter um resultado aproximado, basta mulitplicar o valor 1,944"
                    );
                    p.appendChild(texto);
                    velocidade.appendChild(p);
                    break;
                case 4:
                    input_velocidade_2.value = valor * 1.852;
                    velocidade.innerHTML = "";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(
                        "Para converter de nó para kilometro/hora, divida o valor por 1.852"
                    );
                    p.appendChild(texto);
                    velocidade.appendChild(p);
                    break;
                case 5:
                    input_velocidade_2.value = valor;
                    velocidade.innerHTML = "";
                    break;
            }
            break;
    }
}